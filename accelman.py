
import sched, time, Queue, threading, random, json
import requests
from Adafruit_BNO055 import BNO055

bno = BNO055.BNO055(busnum=3)
if not bno.begin():
    raise RuntimeError('Failed to initialize BNO055! Is the sensor connected?')

status, self_test, error = bno.get_system_status()
print('System status: {0}'.format(status))
print('Self test result (0x0F is normal): 0x{0:02X}'.format(self_test))
if status == 0x01:
    print('System error: {0}'.format(error))
    print('See datasheet section 4.3.59 for the meaning.')

sw, bl, accel, mag, gyro = bno.get_revision()
print('Software version:   {0}'.format(sw))
print('Bootloader version: {0}'.format(bl))
print('Accelerometer ID:   0x{0:02X}'.format(accel))
print('Magnetometer ID:    0x{0:02X}'.format(mag))
print('Gyroscope ID:       0x{0:02X}\n'.format(gyro))

class AccelRecorder(object):
    def __init__(self, bno, interval):
        self._queue = Queue.Queue()
        self._bno = bno
        self._sched = sched.scheduler(time.time, time.sleep)
        self._last_time = 0
        self._interval = interval
        
    def _execute(self):
        next_time = self._last_time + self._interval
        self._sched.enterabs(next_time, 1, self._execute, ())
        accx, accy, accz = self._bno.read_linear_acceleration()
        self._queue.put((self._last_time, accx, accy, accz))
        self._last_time = next_time
        
        # print self._queue.qsize()
    
    def run(self):
        self._last_time = time.time() + self._interval
        self._sched.enterabs(self._last_time, 1, self._execute, ())
        self._sched.run()

def send_to_server(accel_list):
    payload = json.dumps({
        'headers': ('timestamp', 'accx', 'accy', 'accz'),
        'data': accel_list
    })
    # time.sleep(random.random() * 2)
    is_sent = True # random.random() < 0.5
    print len(accel_list), len(payload), is_sent
    requests.post('http://127.0.0.1:5000/accel', data=payload)
    
    return is_sent

accel_recorder = AccelRecorder(bno, 0.01)

accel_recorder_thread = threading.Thread(target=accel_recorder.run)
accel_recorder_thread.setDaemon(True)
accel_recorder_thread.start()

accel_list = []
while True:
    print accel_recorder._queue.qsize()
    while True:
        try:
            accel_list.append(accel_recorder._queue.get_nowait())
        except Queue.Empty:
            break
    print len(accel_list), accel_recorder._queue.qsize()
    if len(accel_list) > 100:
        if send_to_server(accel_list) == True:
            del accel_list[:]
    print '---'
    
    # time.sleep(2)

