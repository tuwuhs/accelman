

from flask import Flask, request
import json

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello, world!"

@app.route('/accel', methods=['POST'])
def accel_post():
    payload = json.loads(request.data)
    accel_list = payload['data']
    print payload['headers']
    print accel_list[0]
    print accel_list[-1]
    print len(accel_list), len(request.data)
    return json.dumps({'status': 'ok'})
    
